﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObject : MonoBehaviour
{
    protected virtual void Start()
    {

    }
    protected virtual void Update()
    {

    }
    protected void Move(GameObject target, float speed)
    {
        if (target != null)
        {
            transform.RotateAround(target.transform.position, target.transform.up, speed * Time.deltaTime);
        }
    }
}
