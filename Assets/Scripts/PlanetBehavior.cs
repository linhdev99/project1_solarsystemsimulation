﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetBehavior : MovingObject
{
    [SerializeField]
    GameObject target;
    [SerializeField]
    float speed = 100f;
    protected override void Start()
    {
        base.Start();
    }
    protected override void Update()
    {
        Move(target, speed);
        base.Update();
    }
}
