﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoonBehavior : MovingObject
{
    [SerializeField]
    GameObject target;
    [SerializeField]
    float speed = 100f;
    protected override void Start()
    {
        GameObject earth = GameObject.Find("Earth");
        if (earth != null)
        {
            this.transform.parent = earth.transform;
        }
        base.Start();
    }
    protected override void Update()
    {
        Move(target, speed);
        base.Update();
    }
}
